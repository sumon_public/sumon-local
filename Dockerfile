FROM rust:1.78.0-bookworm@sha256:5907e96b0293eb53bcc8f09b4883d71449808af289862950ede9a0e3cca44ff5 as builder
################
# https://levelup.gitconnected.com/create-an-optimized-rust-alpine-docker-image-1940db638a6c
##### Builder

# not necessary to install aarch64 target - it would work on arch but not fedora so use docker buildx instead
# install targets for cross-compilation
# RUN rustup target add aarch64-unknown-linux-gnu x86_64-unknown-linux-gnu

# Set the working directory
WORKDIR /usr/src/sumon-local

# my copy Cargo config + cargo fetch to cache dependencies
COPY --link Cargo.lock Cargo.toml /usr/src/sumon-local/
COPY --link dummy.rs .
RUN sed -i 's#src/main.rs#dummy.rs#' Cargo.toml
RUN cargo build --release --bin sumon-local
RUN sed -i 's#dummy.rs#src/main.rs#' Cargo.toml

#RUN cargo fetch

# Copy in the rest of the sources
COPY --link . /usr/src/sumon-local/

## Touch main.rs to prevent cached release build
RUN touch /usr/src/sumon-local/src/main.rs

# This is the actual application build.
RUN cargo build --release --bin sumon-local

################
##### Runtime
# FROM rust:1.78.0-slim-bookworm AS runtime
FROM debian:bookworm-slim@sha256:804194b909ef23fb995d9412c9378fb3505fe2427b70f3cc425339e48a828fca AS runtime

# no need to install openssl anymore, smeagol is freee
# install openssl
#RUN apt-get update && apt-get install -y --no-install-recommends openssl ca-certificates \
#    && apt-get clean && rm -rf /var/lib/apt/lists/*

# Copy application binary from builder image
COPY --from=builder /usr/src/sumon-local/target/release/sumon-local /usr/local/bin

WORKDIR /app

# copy downloaded frontend
# COPY --from=builder /usr/src/sumon-local/target/final-build/frontend frontend

#COPY --link target/final-build/frontend frontend
COPY --link .env .

EXPOSE 8888

# Run the application
CMD ["/usr/local/bin/sumon-local"]
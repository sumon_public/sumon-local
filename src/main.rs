mod controllers;
mod db;
mod devices;
mod ws_client;
mod tests_util;

#[macro_use] extern crate log;

use dotenv::dotenv;
use std::error::Error;
use std::thread;
use tokio::{task, time};

#[tokio::main(worker_threads = 1)]
async fn main() -> Result<(), Box<dyn Error>> {
    dotenv().ok(); // This line loads the environment variables from the ".env" file.

    // https://docs.rs/env_logger/latest/env_logger/#enabling-logging
    // std::env::set_var("RUST_LOG", "info,actix_web=info,actix_server=trace,deadpool.postgres=error");
    env_logger::init();
    info!("concentrator");

    #[allow(unused)]
    let local_id = &std::env::var("LOCAL_ID").expect("Error: LOCAL_ID not found");

    // prepare connection pool
    let pool = db::init_pg_pool();
    let pool2 = pool.clone();
    let client = pool.get().await?;

    // setup DB
    db::init_tables(&client).await?;
    db::ensure_device_types_present(&client).await?;

    // load devices
    // INSERT INTO device VALUES ('45cf1bfd-bc2e-48d7-b3b1-883cbb66f626', 2, 'Zásuvka 1', TRUE, 'HK stůl', 'tele/plug/tasmota_AB9A98/SENSOR', NULL);
    // INSERT INTO device VALUES ('8cec33dc-6c12-4c40-9ffc-5606a9d1b85c', 2, 'Zásuvka 2', TRUE, 'Pracovna', 'tele/plug/tasmota_AB992D/SENSOR', NULL);
    // INSERT INTO device VALUES ('f9bac7af-753e-464a-8402-86a993e2b523', 2, 'Zásuvka 3', TRUE, 'Přenosná', 'tele/plug/tasmota_AB99A8/SENSOR', NULL);

    // start actix-web server
    // std::env::set_var("RUST_LOG", "actix_web=info,actix_server=trace");
    // env_logger::init();
    thread::spawn(move || {
        let _ = controllers::start_server(pool2);
    });

    // run WebSocket client (connects to proxy server)
    task::spawn(async move {
        let proxy_base_url =
            std::env::var("PROXY_BASE_URL").expect("Error: PROXY_BASE_URL not found");
        let proxy_ws_url = std::env::var("PROXY_WS_URL").expect("Error: PROXY_WS_URL not found");
        let concentrator_id = std::env::var("LOCAL_ID").expect("Error: LOCAL_ID not found");
        let private_id = std::env::var("PRIVATE_ID").expect("Error: PRIVATE_ID not found");
        let is_dev = proxy_base_url.eq("http://localhost:8040/");
        loop {
            match ws_client::connect_websocket(
                proxy_base_url.as_str(),
                proxy_ws_url.as_str(),
                concentrator_id.as_str(),
                private_id.as_str(),
                is_dev.clone(),
            )
            .await
            {
                Ok(_) => warn!("ws_client Connection closed"),
                Err(e) => error!("ws_client Error: {}", e),
            }
            time::sleep(time::Duration::from_secs(if is_dev { 1 } else { 10 })).await;
        }
    });

    let device_manager = devices::DeviceManager::new(pool);
    if let Ok(mut device_manager) = device_manager {
        // task::spawn(async move {
        let _ = device_manager.start_fetching().await;
        // });
    };

    panic!("The program should never reach this point");
}

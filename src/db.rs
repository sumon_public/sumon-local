use questdb::ingress::{Buffer, Protocol, Sender, SenderBuilder, TimestampNanos};

use deadpool_postgres::{Client, Manager, ManagerConfig, Pool, RecyclingMethod};
use std::str::FromStr;
use tokio_postgres::NoTls;

use crate::devices::{Device, DeviceType};

pub struct ReadingsEntry {
    pub local_id: String,
    pub device_id: String,
    pub device_label: String,
    pub name: String,
    pub value: f64,
    pub nanoseconds: TimestampNanos,
}

pub fn send_to_db(bf: &mut Buffer) {
    let mut do_send_buffer_to_db = || -> Result<(), anyhow::Error> {
        let host =
            &std::env::var("QUESTDB_INGRESS_HOST").expect("Error: QUESTDB_INGRESS_HOST not found");
        let port: u16 = std::env::var("QUESTDB_INGRESS_PORT")
            .expect("Error: QUESTDB_INGRESS_PORT not found")
            .parse()
            .expect("ERROR: QUESTDB_INGRESS_PORT is not a valid u16");
        let mut sender: Sender = SenderBuilder::new(Protocol::Tcp, host, port).build()?;
        sender.flush(bf)?;
        // dbg!("Buffer sent to QuestDB");
        Ok(())
    };
    if let Err(e) = do_send_buffer_to_db() {
        error!("Failed to send buffer to QuestDB: {}", e);
    }
}

pub fn init_pg_pool() -> Pool {
    let cfg = tokio_postgres::Config::from_str(
        &std::env::var("DATABASE_URL").expect("Error: DATABASE_URL not found"),
    )
    .unwrap();
    let mgr_config = ManagerConfig {
        recycling_method: RecyclingMethod::Fast,
    };
    let mgr = Manager::from_config(cfg, NoTls, mgr_config);
    let pool = Pool::builder(mgr).max_size(16).build().unwrap();
    pool
}

pub async fn init_tables(client: &Client) -> Result<(), Box<dyn std::error::Error>> {
    client
        .query(
            "CREATE TABLE IF NOT EXISTS device_type (id INT NOT NULL, name STRING NOT NULL)",
            &[],
        )
        .await?;
    client
        .query(
            "CREATE TABLE IF NOT EXISTS device (
            id UUID NOT NULL,
            device_type_id INT NOT NULL,
            name STRING NOT NULL,
            enabled BOOLEAN NOT NULL,
            label STRING NULL,
            mqtt_topic STRING NULL,
            config STRING NULL
          )",
            &[],
        )
        .await?;
    client
        .query(
            "CREATE TABLE IF NOT EXISTS config (
            id UUID NOT NULL,
            name STRING NOT NULL,
            position INT NOT NULL,
            config STRING NULL
        )",
            &[],
        )
        .await?;
    client
        .query(
            "CREATE TABLE IF NOT EXISTS readings (
            local_id SYMBOL INDEX,
            device_id SYMBOL INDEX,
            device_label SYMBOL INDEX,
            name SYMBOL INDEX,
            value DOUBLE NOT NULL,
            timestamp TIMESTAMP NOT NULL
        ), INDEX(device_id) timestamp(timestamp) PARTITION BY MONTH WAL;",
            &[],
        )
        .await?;
    Ok(())
}

pub async fn ensure_device_types_present(client: &Client) -> Result<(), Box<dyn std::error::Error>> {
    let device_type_rows = client
        .query("SELECT id, name FROM device_type", &[])
        .await?;
    let device_types: Vec<DeviceType> = device_type_rows
        .iter()
        .map(|row| {
            let id: i32 = row.get("id");
            let name: String = row.get("name");
            DeviceType { id, name }
        })
        .collect();

    for device_type_id in Device::into_iter() {
        let device_type_name = Device::get_type_name(device_type_id);
        let mut exists = false;
        for db_device_type in device_types.iter() {
            if db_device_type.id.eq(&device_type_id) {
                exists = true;
                if !device_type_name.eq(&db_device_type.name) {
                    warn!("DeviceType [{}] name '{}' in the database doesn't match '{}'", &device_type_id, &db_device_type.name, &device_type_name);
                }
            }
        }
        if !exists {
            info!("Adding missing device_type: {} - {}", &device_type_id, &device_type_name);
            client
                .query(
                    "INSERT INTO device_type VALUES ($1, $2);",
                    &[&device_type_id, &device_type_name],
                )
                .await?;
        }
    }

    Ok(())
}
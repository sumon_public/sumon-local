use actix_web::{post, web, Responder, Result};
use std::vec;
use deadpool_postgres::{GenericClient, Pool};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, pg_mapper::TryFromRow)]
pub struct Device {
    id: Uuid,
    device_type_id: i32,
    name: String,
    enabled: bool,
    label: Option<String>,
    mqtt_topic: Option<String>,
    config: Option<String>,
}

#[post("/device-list")]
pub async fn device_list(
    pool: web::Data<Pool>,
) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let conn = pool.get().await?;
    let rows = conn
        .query(
            "SELECT id, device_type_id, name, enabled, label, mqtt_topic, config FROM device ORDER BY device_type_id, id",
            &[],
        )
        .await?;
    let mut devices: Vec<Device> = vec![];
    for row in rows {
        devices.push(Device::try_from(row)?);
    }

    Ok(web::Json(devices))
}

#[post("/device-set")]
pub async fn device_set(
    device: web::Json<Device>,
    pool: web::Data<Pool>,
) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let conn = pool.get().await?;
    let maybe_row = conn.query_opt("SELECT id, device_type_id, name, enabled, label, mqtt_topic, config FROM device WHERE id = $1", &[&device.id.to_string()]).await?;
    match maybe_row {
        Some(_row) => {
            conn.execute(
                &format!(
                    "UPDATE device SET device_type_id = $1, name = $2, enabled = {}, label = $3, mqtt_topic = $4, config = $5 WHERE id = $6",
                    if device.enabled.eq(&true) { &"TRUE" } else { &"FALSE" } // QuestDB has broken postgres implementation bool doesnt work with binded params
                ),
                &[&device.device_type_id, &device.name, &device.label, &device.mqtt_topic, &device.config, &device.id.to_string()]
            ).await?;
        }
        None => {
            conn.execute(
                &format!(
                    "INSERT INTO device (id, device_type_id, name, enabled, label, mqtt_topic, config) VALUES ($1, $2, $3, {}, $4, $5, $6)",
                    if device.enabled.eq(&true) { &"TRUE" } else { &"FALSE" } // QuestDB has broken postgres implementation bool doesnt work with binded params
                ),
                &[&device.id, &device.device_type_id, &device.name, &device.label, &device.mqtt_topic, &device.config]
            ).await?;
        }
    };

    let row = conn.query_one("SELECT id, device_type_id, name, enabled, label, mqtt_topic, config FROM device WHERE id = $1", &[&device.id.to_string()]).await?;
    let final_device: Device = Device::try_from(row)?;

    Ok(web::Json(final_device))
}

#[derive(Serialize, Deserialize)]
pub struct DeviceId {
    id: Uuid,
}

#[post("/device-remove")]
pub async fn device_remove(
    device: web::Json<DeviceId>,
    pool: web::Data<Pool>,
) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let mut conn = pool.get().await?;
    // return Error when row is not found
    let row = conn.query_one("SELECT id, device_type_id, name, enabled, label, mqtt_topic, config FROM device WHERE id = $1", &[&device.id.to_string()]).await?;

    // QuestDb doesn't support DELETE so there is this ugly workaround (there shouldn't be many devices, so this uneffectivenes shouldn't matter)
    let transaction = conn.transaction().await?;
    // binded parameter doesn't work here (QuestDB probably supports them in DML only)
    transaction
        .execute(
            &format!(
                "CREATE TABLE device_tmp AS (SELECT * FROM device WHERE id != '{}')",
                &device.id.to_string()
            ),
            &[],
        )
        .await?;
    transaction.execute("DROP TABLE device", &[]).await?;
    transaction
        .execute("RENAME TABLE device_tmp TO device", &[])
        .await?;
    transaction.commit().await?;

    Ok(web::Json(Device::try_from(row)?))
}

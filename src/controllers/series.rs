use actix_web::{post, web, Responder, Result};
use chrono::{DateTime, Duration, FixedOffset, NaiveDateTime, Utc};
use deadpool_postgres::Pool;
use serde::Serialize;
use serde_json::{json, Map};
use sumon_common::{SeriesRequest, SeriesRequestField, SeriesResponse, TimeSeriesFieldUnit, TimeSeriesSampleFill, TimeSeriesSampleUnit};
use tokio_postgres::Row;
use std::{collections::{BTreeMap, HashMap}, vec};

use crate::tests_util::DbClient;

#[derive(Serialize, Clone, pg_mapper::TryFromRow)]
pub struct ReadingRow {
    timestamp: NaiveDateTime,
    device_label: String,
    name: String,
    value: f64,
}

#[post("/series")]
pub async fn series_action(
    series_requests: web::Json<Vec<SeriesRequest>>,
    pool: web::Data<Pool>,
) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let conn = pool.get().await?;

    let mut response: Vec<SeriesResponse<serde_json::value::Value>> = vec![];
    for series_request in series_requests.iter() {
        let time_offset = series_request.start_time.offset();
        let mut series_map: BTreeMap<DateTime<FixedOffset>, Map<String, serde_json::value::Value>> = BTreeMap::new();
        for field_request in &series_request.fields {
            for row in match &series_request.sample_unit {
                TimeSeriesSampleUnit::Hour => fetch_series_sample_hour(&conn, &series_request, &field_request).await?,
                TimeSeriesSampleUnit::Day => fetch_series_sample_day(&conn, &series_request, &field_request).await?,
                _ => unimplemented!("Only hour and day sampling is implemented in series fetch."),
            } {
                // cannot get DateTime<FixedOffset> directly
                // tokio-postgres won't handle datetimes with local timezone -> we must use UTC or string
                let timestamp_utc: NaiveDateTime = match row.try_get("timestamp_utc") {
                    Ok(timestamp_utc) => timestamp_utc,
                    Err(e) => { error!("Couldn't read timestamp column: {}", e); continue },
                };

                // convert timestamp_utc to timezoned DateTime<FixedOffset>
                let timestamp: DateTime<FixedOffset> = DateTime::from_naive_utc_and_offset(timestamp_utc, time_offset.clone());

                if !series_map.contains_key(&timestamp) {
                    series_map.insert(timestamp.clone(), Map::new());
                }
                let data_map = match series_map.get_mut(&timestamp) {
                    Some(tmp_chunk) => tmp_chunk,
                    None => continue,
                };

                // insert timestamp as x
                data_map.insert("x".to_string(), json!(timestamp.to_rfc3339()));

                let value: f64 = match row.try_get("value") {
                    Ok(value) => value,
                    Err(_) => { error!("Couldn't read value column"); continue },
                };
                let value = match &field_request.unit {
                    TimeSeriesFieldUnit::WattHour => {
                        value * match &series_request.sample_unit {
                            TimeSeriesSampleUnit::Second => 1.0/3600.0,
                            TimeSeriesSampleUnit::Minute => 1.0/60.0,
                            TimeSeriesSampleUnit::Hour => 1.0,
                            TimeSeriesSampleUnit::Day => 24.0,
                            TimeSeriesSampleUnit::Month => 30.0*24.0,
                            TimeSeriesSampleUnit::Year => 365.0*24.0,
                            _ => unimplemented!("Given sample_unit is not implemented for WattHours value type."),
                        }
                    },
                    _ => value,
                };
                data_map.insert(field_request.as_name.clone(), json!(value));

                match field_request.device_label_as.clone() {
                    Some(device_label_field) => {
                        let device_label: String = match row.try_get("device_label") {
                            Ok(value) => value,
                            Err(_) => { error!("Couldn't read device_label column"); continue },
                        };
                        data_map.insert(device_label_field.clone(), json!(device_label));
                    },
                    None => {},
                }
            }
        }

        let keys: Vec<String> = series_map.keys().map(|date_time| date_time.to_rfc3339()).collect();
        let keys_obj = json!(keys);
        debug!("Fetched timestamps: {}", serde_json::to_string_pretty(&keys_obj).unwrap());

        let mut default_values_cache: HashMap<String, serde_json::value::Value> = HashMap::new();
        series_data_post_process(&conn, &mut series_map, &mut default_values_cache, series_request).await?;

        // convert series_map values into Vec<serde_json_value::Value::Object>
        let data = series_map.into_values().map(|data_map| json!(data_map.to_owned())).collect();
        
        response.push(SeriesResponse {
            name: series_request.name.clone(),
            sample_rate: series_request.sample_rate.clone(),
            sample_unit: series_request.sample_unit.clone(),
            data: data
        });
    }

    Ok(web::Json(response))
}

async fn fetch_series_sample_hour(
    conn: &deadpool_postgres::Object,
    series_request: &SeriesRequest,
    field_request: &SeriesRequestField
) -> Result<Vec<Row>, Box<dyn std::error::Error>> {
    debug!("Time series query:
        SELECT 
            timestamp AS timestamp_utc,
            device_label AS device_label,
            {}(value) AS value
        FROM 
            readings
        WHERE
            device_id = to_uuid('{}')
            AND name = '{}'
            AND timestamp >= to_utc('{}', '{}')
            AND timestamp < to_utc('{}', '{}')
        SAMPLE BY {}{}
        FILL({});",
        serde_json::to_value(&field_request.aggregation).unwrap_or(json!("AVG")).as_str().unwrap_or("AVG"),
        &field_request.device_id.to_string(),
        &field_request.field,
        &series_request.start_time.naive_local(),
        &series_request.start_time.offset().to_string(),
        &series_request.end_time.naive_local(),
        &series_request.start_time.offset().to_string(),
        series_request.sample_rate,
        serde_json::to_value(&series_request.sample_unit).unwrap_or(json!("h")).as_str().unwrap_or("h"),
        serde_json::to_value(&field_request.fill).unwrap_or(json!("0.0")).as_str().unwrap_or("0.0"),
    );

    debug!("Time offset: {}", series_request.start_time.offset().to_string());
    debug!("Timestamp naive START: {}", &series_request.start_time.naive_local());
    debug!("Timestamp naive END: {}", &series_request.end_time.naive_local());

    let rows = match conn.query(&format!(
            "SELECT 
                timestamp AS timestamp_utc,
                device_label AS device_label,
                {}(value) AS value
            FROM 
                readings
            WHERE
                device_id = $2
                AND name = $3
                AND timestamp >= to_utc($4, $1)
                AND timestamp < to_utc($5, $1)
            SAMPLE BY {}{}
            FILL({});",
            serde_json::to_value(&field_request.aggregation).unwrap_or(json!("AVG")).as_str().unwrap_or("AVG"),
            series_request.sample_rate,
            serde_json::to_value(&series_request.sample_unit).unwrap_or(json!("h")).as_str().unwrap_or("h"),
            serde_json::to_value(&field_request.fill).unwrap_or(json!("0.0")).as_str().unwrap_or("0.0"),
        ),
        &[
            &series_request.start_time.offset().to_string(),
            &field_request.device_id.to_string(),
            &field_request.field,
            &series_request.start_time.naive_local(),
            &series_request.end_time.naive_local(),
        ],
    ).await {
        Ok(rows) => rows,
        Err(e) => { error!("Series query error: {}", e); return Err(Box::new(e)); },
    };
    debug!("Number of rows for field {} as {}: {}", &field_request.field, &field_request.as_name, rows.len());

    Ok(rows)
}

async fn fetch_series_sample_day(
    conn: &deadpool_postgres::Object,
    series_request: &SeriesRequest,
    field_request: &SeriesRequestField
) -> Result<Vec<Row>, Box<dyn std::error::Error>> {
    debug!("Time offset: {}", series_request.start_time.offset().to_string());
    debug!("Timestamp naive START: {}", &series_request.start_time.naive_local());
    debug!("Timestamp naive END: {}", &series_request.end_time.naive_local());

    let rows = match conn.query(&format!(
            "WITH hourly_data AS (
                SELECT 
                    to_timezone(timestamp, $1) AS timestamp_local,
                    device_label AS device_label,
                    COALESCE({}(value), 0.0) AS aggregated_hourly_value
                FROM readings
                WHERE
                    device_id = $2
                    AND name = $3
                    AND timestamp >= to_utc($4, $1)
                    AND timestamp < to_utc($5, $1)
                SAMPLE BY 1h
                FILL({})
            )
            SELECT
                date_trunc('day', timestamp_local) AS timestamp_local,
                to_utc(date_trunc('day', timestamp_local), $1) AS timestamp_utc,
                hourly_data.device_label AS device_label,
                {}(aggregated_hourly_value) AS value
            FROM hourly_data
            GROUP BY timestamp_local, device_label
            ORDER BY timestamp_local;",
            serde_json::to_value(&field_request.aggregation).unwrap_or(json!("AVG")).as_str().unwrap_or("AVG"),
            serde_json::to_value(&field_request.fill).unwrap_or(json!("0.0")).as_str().unwrap_or("0.0"),
            serde_json::to_value(&field_request.aggregation).unwrap_or(json!("AVG")).as_str().unwrap_or("AVG"),
        ),
        &[
            &series_request.start_time.offset().to_string(),
            &field_request.device_id.to_string(),
            &field_request.field,
            &series_request.start_time.naive_local(),
            &series_request.end_time.naive_local(),
        ],
    ).await {
        Ok(rows) => rows,
        Err(e) => { error!("Series query error: {}", e); return Err(Box::new(e)); },
    };
    debug!("Number of rows for field {} as {}: {}", &field_request.field, &field_request.as_name, rows.len());

    Ok(rows)
}

/// add missing timestamps and fill default previous/device_label values
async fn series_data_post_process<D>(
    conn: &D,
    map: &mut BTreeMap<DateTime<FixedOffset>, Map<String, serde_json::value::Value>>,
    default_values_cache: &mut HashMap<String, serde_json::value::Value>,
    series_request: &SeriesRequest,
) -> Result<(), Box<dyn std::error::Error>>
where
    D: DbClient + Send + Sync,
{
    let mut current_time = series_request.start_time;
    let end_time = series_request.end_time;
    let sample_duration = match series_request.sample_unit {
        TimeSeriesSampleUnit::Microsecond => Duration::microseconds(series_request.sample_rate),
        TimeSeriesSampleUnit::Millisecond => Duration::milliseconds(series_request.sample_rate),
        TimeSeriesSampleUnit::Second => Duration::seconds(series_request.sample_rate),
        TimeSeriesSampleUnit::Minute => Duration::minutes(series_request.sample_rate),
        TimeSeriesSampleUnit::Hour => Duration::hours(series_request.sample_rate),
        TimeSeriesSampleUnit::Day => Duration::days(series_request.sample_rate),
        TimeSeriesSampleUnit::Month => Duration::days(30 * series_request.sample_rate), // Approximation
        TimeSeriesSampleUnit::Year => Duration::days(365 * series_request.sample_rate), // Approximation
    };

    while current_time < end_time {
        if !map.contains_key(&current_time) {
            let mut data_map = Map::new();
            data_map.insert("x".to_string(), json!(current_time.to_rfc3339()));
            map.insert(current_time, data_map);
        }
        current_time = current_time + sample_duration;
    }

    let now_utc: DateTime<Utc> = Utc::now();
    let now_tz: DateTime<FixedOffset> = now_utc.with_timezone(&series_request.start_time.offset());
    for (timestamp, data_map) in map.iter_mut() {
        if series_request.end_time.lt(timestamp) {
            error!("Database fetched unnecessary data ({}) for series: {:?}", &timestamp, &series_request);
            continue;
        }
        if now_tz.le(timestamp) {
            continue; // skip filling future data
        }
        for field_request in &series_request.fields {
            if !data_map.contains_key(&field_request.as_name) {
                // zero values might be skipped - these are handled on the other side to reduce unnecessary data transfer
                // previous default values has to be fetched from database
                // eg. BatterySoc might not be present at the start of the series and filling 0 is not relevant,
                // therefore seek for newest entry even before start_time
                if field_request.fill.eq(&TimeSeriesSampleFill::PREV) {
                    if !default_values_cache.contains_key(&field_request.as_name) {
                        let previous_default_value = fetch_default_previous_number(conn, &field_request, series_request.start_time).await.unwrap_or(json!(0.0));
                        default_values_cache.insert(field_request.as_name.clone(), json!(previous_default_value));
                    }
                    data_map.insert(field_request.as_name.clone(), default_values_cache.get(&field_request.as_name).unwrap_or(&json!(0.0)).clone());
                }
            }

            if let Some(device_label_field) = &field_request.device_label_as {
                if !data_map.contains_key(device_label_field) {
                    if !default_values_cache.contains_key(device_label_field) {
                        let device_label = fetch_default_device_label(conn, &field_request, series_request.end_time).await.unwrap_or("".to_string());
                        default_values_cache.insert(device_label_field.clone(), json!(device_label));
                    }
                    data_map.insert(device_label_field.clone(), default_values_cache.get(device_label_field).unwrap_or(&json!("")).clone());
                }
            }
        }
    }

    Ok(())
}

async fn fetch_default_previous_number<D>(
    conn: &D,
    field_request: &SeriesRequestField,
    start_time: DateTime<FixedOffset>,
) -> Result<serde_json::value::Value, Box<dyn std::error::Error>>
where
    D: DbClient + Send + Sync,
{
    let default_number: f64 = conn
        .query_one(
            "SELECT value
                FROM readings
                WHERE device_id = $1 AND name = $2 AND timestamp < to_utc($3, $4)
                LATEST ON timestamp PARTITION BY name;",
    &[
                &field_request.device_id.to_string(),
                &field_request.field,
                &start_time.naive_local(),
                &start_time.offset().to_string()
            ],
        )
        .await?
        .try_get("value")
        .unwrap_or(0.0);
    Ok(json!(default_number))
}

async fn fetch_default_device_label<D>(
    conn: &D,
    field_request: &SeriesRequestField,
    start_time: DateTime<FixedOffset>,
) -> Result<String, Box<dyn std::error::Error>>
where
    D: DbClient + Send + Sync,
{
    let device_label: String = conn
        .query_one(
            "SELECT device_label
                FROM readings
                WHERE device_id = $1 AND name = $2 AND timestamp < to_utc($3, $4)
                LATEST ON timestamp PARTITION BY name;",
            &[
                &field_request.device_id.to_string(),
                &field_request.field,
                &start_time.naive_local(),
                &start_time.offset().to_string()
            ],
        )
        .await?
        .try_get("device_label")
        .unwrap_or("".to_string());
    Ok(device_label)
}


#[cfg(test)]
mod tests {
    use chrono::DateTime;
    use sumon_common::TimeSeriesFieldAggregation;
    use uuid::Uuid;

    use crate::tests_util::MockDbClient;

    use super::*;

    /// Test that fn series_data_post_process() properly fills missing timestamps and necessary default values
    #[tokio::test]
    async fn test_series_data_post_process() {
        let mock_db_client = MockDbClient {};
        let mut series_map = BTreeMap::new();
        let mut default_values_cache = HashMap::new();
        let series_request = SeriesRequest {
            name: "test".to_string(),
            start_time: DateTime::<FixedOffset>::parse_from_rfc3339("2024-06-23T00:00:00+02:00").unwrap(),
            end_time: DateTime::<FixedOffset>::parse_from_rfc3339("2024-06-23T05:00:00+02:00").unwrap(),
            sample_rate: 1,
            sample_unit: TimeSeriesSampleUnit::Hour,
            fields: vec![
                SeriesRequestField {
                    device_id: Uuid::parse_str("3f3d2c63-684d-4487-99f4-cd6d1f0f1320").unwrap(),
                    field: "PvPower".to_string(),
                    as_name: "solar".to_string(),
                    aggregation: TimeSeriesFieldAggregation::AVG,
                    unit: TimeSeriesFieldUnit::WattHour,
                    fill: TimeSeriesSampleFill::ZERO,
                    device_label_as: Some("device_label".to_string()),
                },
                SeriesRequestField {
                    device_id: Uuid::parse_str("3f3d2c63-684d-4487-99f4-cd6d1f0f1320").unwrap(),
                    field: "BatterySoc".to_string(),
                    as_name: "battery_soc".to_string(),
                    aggregation: TimeSeriesFieldAggregation::AVG,
                    unit: TimeSeriesFieldUnit::Percentage,
                    fill: TimeSeriesSampleFill::PREV,
                    device_label_as: None,
                }
            ],
        };

        let mut data_map = Map::new();
        data_map.insert("battery_soc".to_string(), json!(64.0));
        series_map.insert(DateTime::<FixedOffset>::parse_from_rfc3339("2024-06-23T03:00:00+02:00").unwrap(), data_map);
        let mut data_map = Map::new();
        data_map.insert("solar".to_string(), json!(25.5));
        data_map.insert("device_label".to_string(), json!("Victron"));
        data_map.insert("battery_soc".to_string(), json!(62.0));
        series_map.insert(DateTime::<FixedOffset>::parse_from_rfc3339("2024-06-23T04:00:00+02:00").unwrap(), data_map);
        let mut data_map = Map::new();
        data_map.insert("solar".to_string(), json!(46.5));
        data_map.insert("device_label".to_string(), json!("Victron"));
        data_map.insert("battery_soc".to_string(), json!(61.0));
        series_map.insert(DateTime::<FixedOffset>::parse_from_rfc3339("2024-06-23T05:00:00+02:00").unwrap(), data_map);

        // pre-cache default values so test won't call mocked database client
        default_values_cache.insert("device_label".to_string(), json!("Victron"));
        default_values_cache.insert("battery_soc".to_string(), json!(65.0));

        series_data_post_process(&mock_db_client, &mut series_map, &mut default_values_cache, &series_request)
            .await
            .expect("Failed to process series data");

        assert_eq!(series_map.len(), 6);
        assert!(series_map.contains_key(&DateTime::<FixedOffset>::parse_from_rfc3339("2024-06-23T00:00:00+02:00").unwrap()));
        assert!(series_map.contains_key(&DateTime::<FixedOffset>::parse_from_rfc3339("2024-06-23T01:00:00+02:00").unwrap()));
        assert!(series_map.contains_key(&DateTime::<FixedOffset>::parse_from_rfc3339("2024-06-23T02:00:00+02:00").unwrap()));
        assert!(series_map.contains_key(&DateTime::<FixedOffset>::parse_from_rfc3339("2024-06-23T03:00:00+02:00").unwrap()));
        assert!(series_map.contains_key(&DateTime::<FixedOffset>::parse_from_rfc3339("2024-06-23T04:00:00+02:00").unwrap()));
        assert!(series_map.contains_key(&DateTime::<FixedOffset>::parse_from_rfc3339("2024-06-23T05:00:00+02:00").unwrap()));

        let mut expected_battery_soc_values: Vec<f64> = vec![61.0, 62.0, 64.0, 65.0, 65.0, 65.0];
        let mut expected_solar_values: Vec<f64> = vec![46.5, 25.5];
        for (timestamp, data_map) in series_map.iter() {
            assert!(data_map.contains_key("device_label"));
            assert_eq!(data_map.get("device_label").unwrap(), &json!("Victron"));
            assert!(data_map.contains_key("battery_soc"));
            assert_eq!(data_map.get("battery_soc").unwrap(), &json!(expected_battery_soc_values.pop()));
            if timestamp.lt(&DateTime::<FixedOffset>::parse_from_rfc3339("2024-06-23T04:00:00+02:00").unwrap()) {
                assert!(!data_map.contains_key("solar"));
            } else {
                assert!(data_map.contains_key("solar"));
                assert_eq!(data_map.get("solar").unwrap(), &json!(expected_solar_values.pop()));
            }
        }
    }
}
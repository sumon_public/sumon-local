use actix_web::{post, web, Responder, Result};
use chrono::NaiveDateTime;
use deadpool_postgres::Pool;
use serde::{Deserialize, Serialize};
use serde_with::serde_as;
use std::vec;

#[derive(Serialize, Clone, pg_mapper::TryFromRow)]
pub struct Reading {
    timestamp: NaiveDateTime,
    local_id: String,
    device_id: String,
    name: String,
    value: f64,
}

#[derive(Deserialize)]
#[allow(dead_code)]
pub struct ReadingsFilter {
    devices: Option<Vec<DeviceFilter>>,
    time: Option<TimeFilter>,
}

#[derive(Deserialize)]
#[allow(dead_code)]
pub struct DeviceFilter {
    id: String,
    types: Option<Vec<String>>,
}

#[derive(Deserialize)]
#[serde_as]
pub struct TimeFilter {
    #[serde_as(as = "chrono::DateTime<chrono::Utc>")]
    start_time: NaiveDateTime,
    #[serde_as(as = "chrono::DateTime<chrono::Utc>")]
    end_time: NaiveDateTime,
}

#[post("/readings-list")]
pub async fn readings_list(
    filter: web::Json<ReadingsFilter>,
    pool: web::Data<Pool>,
) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let conn = pool.get().await?;
    let maybe_time_filter = &filter.time;
    let maybe_devices_filter = &filter.devices;
    let rows = match maybe_time_filter {
        Some(filter_time) => match maybe_devices_filter {
            Some(_devices) => {
                conn.query(
                    "SELECT timestamp, local_id, device_id, name, value FROM readings
                        WHERE timestamp >= $1 AND timestamp <= $2
                        LIMIT 100000",
                    &[&filter_time.start_time, &filter_time.end_time],
                )
                .await?
            }
            None => {
                conn.query(
                    "SELECT timestamp, local_id, device_id, name, value FROM readings
                        WHERE timestamp >= $1 AND timestamp <= $2
                        LIMIT 100000",
                    &[&filter_time.start_time, &filter_time.end_time],
                )
                .await?
            }
        },
        None => {
            conn.query(
                "SELECT timestamp, local_id, device_id, name, value FROM readings LIMIT 100000",
                &[],
            )
            .await?
        }
    };

    let mut readings: Vec<Reading> = vec![];
    for row in rows {
        readings.push(Reading::try_from(row)?);

        // test large data
        // let reading = Reading::try_from(row)?;
        // let mut count = 0u32;
        // loop {
        //     count += 1;
        //     readings.push(reading.clone());
        //     if count > 1000 {
        //         break;
        //     }
        // }
    }

    Ok(web::Json(readings))
}

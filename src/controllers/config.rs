use actix_web::{
    error::{ErrorBadRequest, ErrorNotFound},
    post, web, Responder, Result,
};
use deadpool_postgres::{GenericClient, Pool};
use serde::{Deserialize, Serialize};
use std::vec;
use uuid::Uuid;

#[derive(Serialize, Deserialize, pg_mapper::TryFromRow)]
pub struct Config {
    id: Uuid,
    name: String,
    position: i32,
    config: Option<String>,
}

#[post("/config-list")]
pub async fn config_list(
    pool: web::Data<Pool>,
) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let conn = pool.get().await?;
    let rows = conn
        .query(
            "SELECT id, name, position, config FROM config ORDER BY position, name",
            &[],
        )
        .await?;
    let mut configs: Vec<Config> = vec![];
    for row in rows {
        configs.push(Config::try_from(row)?);
    }

    Ok(web::Json(configs))
}

#[derive(Serialize, Deserialize, pg_mapper::TryFromRow)]
pub struct ConfigRequest {
    id: Option<Uuid>,
    name: String,
    position: Option<i32>,
    config: Option<String>,
}

#[post("/config-set")]
pub async fn config_set(
    mut config: web::Json<ConfigRequest>,
    pool: web::Data<Pool>,
) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let conn = pool.get().await?;
    if (&config.position).is_none() {
        config.position = Some(0i32);
    }
    // find by id or fallback to name
    let maybe_row = match config.id {
        Some(id) => {
            conn.query_opt(
                "SELECT id, name, position, config FROM config WHERE id = $1",
                &[&id.to_string()],
            )
            .await?
        }
        None => {
            conn.query_opt(
                "SELECT id, name, position, config FROM config WHERE name = $1",
                &[&config.name.to_string()],
            )
            .await?
        }
    };

    let id;
    match maybe_row {
        Some(row) => {
            let temp_config = Config::try_from(row)?;
            id = temp_config.id;
            conn.execute(
                "UPDATE config SET name = $1, position = $2, config = $3 WHERE id = $4",
                &[
                    &config.name,
                    &config.position,
                    &config.config,
                    &temp_config.id.to_string(),
                ], // always use safe id for condition
            )
            .await?;
        }
        None => {
            id = if config.id.is_some() {
                config.id.unwrap()
            } else {
                Uuid::new_v4()
            };
            conn.execute(
                "INSERT INTO config (id, name, position, config) VALUES ($1, $2, $3, $4)",
                &[&id, &config.name, &config.position, &config.config],
            )
            .await?;
        }
    };

    let row = conn
        .query_one(
            "SELECT id, name, position, config FROM config WHERE id = $1",
            &[&id.to_string()],
        )
        .await?;
    let final_obj: Config = Config::try_from(row)?;

    Ok(web::Json(final_obj))
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ConfigId {
    id: Option<Uuid>,
    name: Option<String>,
}

#[post("/config-get")]
pub async fn config_get(
    config_id: web::Json<ConfigId>,
    pool: web::Data<Pool>,
) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let conn = pool.get().await?;
    if config_id.id.clone().is_none() && config_id.name.clone().is_none() {
        return Err(Box::new(ErrorBadRequest(
            "Invalid request: id or name must be present",
        ))); // id or name is required
    }
    // return Error when row is not found
    // find by id or fallback to name
    let mut maybe_row = match config_id.id {
        Some(id) => {
            conn.query_opt(
                "SELECT id, name, position, config FROM config WHERE id = $1",
                &[&id.to_string()],
            )
            .await?
        }
        None => {
            conn.query_opt(
                "SELECT id, name, position, config FROM config WHERE name = $1",
                &[&config_id.name.clone().unwrap().to_string()],
            )
            .await?
        }
    };

    // try create new row if name was supplied
    if maybe_row.is_none() && config_id.name.is_some() {
        let id = if config_id.id.is_some() {
            config_id.id.unwrap()
        } else {
            Uuid::new_v4()
        };
        let name = config_id.clone().name.unwrap();
        conn.execute(
            "INSERT INTO config (id, name, position, config) VALUES ($1, $2, $3, $4)",
            &[&id, &name, &Some(0i32), &Some("{}")],
        )
        .await?;
        maybe_row = Some(
            conn.query_one(
                "SELECT id, name, position, config FROM config WHERE id = $1",
                &[&id.to_string()],
            )
            .await?,
        );
    } else if maybe_row.is_none() {
        return Err(Box::new(ErrorNotFound(
            "Config with specified id was not found",
        )));
    }
    if maybe_row.is_none() {
        return Err(Box::new(ErrorNotFound(
            "Config with specified id or name was not found and couldn't be created",
        )));
    }

    let config = Config::try_from(maybe_row.unwrap())?;

    Ok(web::Json(config))
}

#[post("/config-remove")]
pub async fn config_remove(
    config_id: web::Json<ConfigId>,
    pool: web::Data<Pool>,
) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let mut conn = pool.get().await?;
    if config_id.id.clone().is_none() && config_id.name.clone().is_none() {
        return Err(Box::new(ErrorBadRequest(
            "Invalid request: id or name must be present",
        ))); // id or name is required
    }
    // return Error when row is not found
    // find by id or fallback to name
    let row = match config_id.id {
        Some(id) => {
            conn.query_one(
                "SELECT id, name, position, config FROM config WHERE id = $1",
                &[&id.to_string()],
            )
            .await?
        }
        None => {
            conn.query_one(
                "SELECT id, name, position, config FROM config WHERE name = $1",
                &[&config_id.name.clone().unwrap().to_string()],
            )
            .await?
        }
    };
    let config = Config::try_from(row)?;

    // QuestDb doesn't support DELETE so there is this ugly workaround (there shouldn't be many rows, so this uneffectivenes shouldn't matter)
    let transaction = conn.transaction().await?;
    // binded parameter doesn't work here (QuestDB probably supports them in DML only)
    transaction
        .execute(
            &format!(
                "CREATE TABLE config_tmp AS (SELECT * FROM config WHERE id != '{}')",
                &config.id.to_string()
            ),
            &[],
        )
        .await?;
    transaction.execute("DROP TABLE config", &[]).await?;
    transaction
        .execute("RENAME TABLE config_tmp TO config", &[])
        .await?;
    transaction.commit().await?;

    Ok(web::Json(config))
}

mod config;
mod device;
mod readings;
mod series;

use actix_cors::Cors;
pub use device::device_list;
pub use device::device_remove;
pub use device::device_set;
pub use readings::readings_list;
pub use series::series_action;

pub use config::config_get;
pub use config::config_list;
pub use config::config_remove;
pub use config::config_set;

use actix_web::{get, http::header::ContentType, web, App, HttpResponse, HttpServer};
use deadpool_postgres::Pool;

#[actix_web::main]
pub async fn start_server(pool: Pool) -> std::io::Result<()> {
    let pool = pool.clone();
    let d_pool = web::Data::new(pool);
    let port: u16 = std::env::var("LISTEN_PORT").expect("Error: LISTEN_PORT not found").parse().unwrap();
    HttpServer::new(move || {
        App::new()
            .app_data(d_pool.clone())
            .wrap(Cors::permissive())
            .service(readings_list)
            .service(series_action)
            .service(device_list)
            .service(device_set)
            .service(device_remove)
            .service(config_list)
            .service(config_set)
            .service(config_get)
            .service(config_remove)
            .service(index)
    })
    .disable_signals()
    .workers(2)
    .bind((std::env::var("LISTEN_IP").expect("Error: LISTEN_IP not found"), port))?
    .run()
    .await
}

#[get("/")]
async fn index() -> HttpResponse {
    HttpResponse::Ok()
        .content_type(ContentType::plaintext())
        .body("local concentrator webserver running")
}

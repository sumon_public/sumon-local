mod mqtt_manager;
mod tasmota_plug;
mod victron;

use bytes::Bytes;
use deadpool_postgres::Pool;
use questdb::ingress::Buffer;
use serde::{Deserialize, Serialize};
use tokio::sync::mpsc::{self, Receiver, Sender};
use std::sync::Arc;
use std::time::Duration;
use std::{collections::HashMap, error::Error};
use tokio::{sync::Mutex, task, time};
use uuid::Uuid;

use crate::db::{self, ReadingsEntry};

use self::mqtt_manager::MqttManager;
use self::tasmota_plug::{TasmotaPlug, TasmotaPlugConfig};
use self::victron::{Victron, VictronConfig};

#[derive(Serialize, Deserialize, Debug)]
pub struct DeviceType {
    pub id: i32,
    pub name: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct DeviceCommon {
    id: Uuid,
    device_type_id: i32,
    device_type_name: String,
    name: String,
    enabled: bool,
    label: Option<String>,
    mqtt_topic: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
#[serde(tag = "type")] // You can use this attribute to add a type tag in the serialized data
pub enum DeviceConfig {
    Victron(VictronConfig),
    TasmotaPlug(TasmotaPlugConfig),
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub enum Device {
    Victron(Victron),
    TasmotaPlug(TasmotaPlug),
}

impl Device {
    pub fn into_iter() -> core::array::IntoIter<i32, 2> {
        [
            victron::ID,
            tasmota_plug::ID,
        ]
        .into_iter()
    }

    pub fn from_config(device_type_id: i32, common: DeviceCommon, config_json: Option<String>) -> Option<Device> {
        match device_type_id {
            victron::ID => Victron::from_config(common, config_json.as_deref()),
            tasmota_plug::ID => TasmotaPlug::from_config(common, config_json.as_deref()),
            _ => {
                error!("Device type ID not found: {}", &device_type_id);
                None // handle other device types
            },
        }
    }

    pub fn get_type_name(device_type_id: i32) -> &'static str {
        match device_type_id {
            victron::ID => victron::NAME,
            tasmota_plug::ID => tasmota_plug::NAME,
            _ => "unknown",
        }
    }

    // generic usage of trait functions doesn't seem to work with async yet
    // pub fn initialize<'a>(&'a self, mqtt_manager: &'a mut MqttManager) -> Box<dyn Future<Output = ()> + 'a> {
    pub async fn initialize(&self, mqtt_manager: &mut MqttManager) -> Result<(), ()> {
        match self {
            Device::Victron(device) => device.initialize(mqtt_manager).await,
            Device::TasmotaPlug(device) => device.initialize(mqtt_manager).await,
        }
    }

    pub async fn close(&self, mqtt_manager: &mut MqttManager) {
        match self {
            Device::Victron(device) => device.close(mqtt_manager).await,
            Device::TasmotaPlug(device) => device.close(mqtt_manager).await,
        }
    }

    pub async fn incoming(&self, topic: &str, message: &Bytes, readings_tx: &Sender<db::ReadingsEntry>) {
        match self {
            Device::Victron(device) => device.incoming(topic, message, readings_tx).await,
            Device::TasmotaPlug(device) => device.incoming(topic, message, readings_tx).await,
        }
    }

    pub fn get_id(&self) -> Uuid {
        match self {
            Device::Victron(device) => device.common.id.clone(),
            Device::TasmotaPlug(device) => device.common.id.clone(),
        }
    }

    pub fn get_common(&self) -> DeviceCommon {
        match self {
            Device::Victron(device) => device.common.clone(),
            Device::TasmotaPlug(device) => device.common.clone(),
        }
    }
}

trait DeviceBehavior {
    fn from_config(common: DeviceCommon, config_json: Option<&str>) -> Option<Device>;
    async fn initialize(&self, mqtt_manager: &mut MqttManager) -> Result<(), ()>;
    async fn close(&self, mqtt_manager: &mut MqttManager);
    async fn incoming(&self, topic: &str, message: &Bytes, readings_tx: &Sender<db::ReadingsEntry>);
}

pub struct DeviceManager {
    pool: Pool,
    devices: Arc<Mutex<HashMap<Uuid, Device>>>,
    #[allow(unused)]
    readings_tx: Sender<db::ReadingsEntry>,
    readings_rx: Option<Receiver<db::ReadingsEntry>>,
    mqtt_manager: Arc<Mutex<MqttManager>>,
}

impl DeviceManager {
    pub fn new(pool: Pool) -> Result<Self, Box<dyn Error>> {
        let devices: HashMap<Uuid, Device> = HashMap::new();
        let devices = Arc::new(Mutex::new(devices));
        let (readings_tx, readings_rx) = mpsc::channel(100);
        Ok(DeviceManager {
            pool,
            devices,
            readings_tx: readings_tx.clone(),
            readings_rx: Some(readings_rx),
            mqtt_manager: Arc::new(Mutex::new(MqttManager::new(readings_tx.clone()))),
        })
    }

    pub async fn start_fetching(&mut self) -> Result<(), Box<dyn Error>> {
        let db_pool = self.pool.clone();
        let mut client = self.pool.get().await?;
        let devices_arc = self.devices.clone();
        let mqtt_manager = self.mqtt_manager.clone();
        

        // send buffered data periodically via Quest DB ingress (InfluxDB Line Protocol - ILP) protocol
        // let readings_buffer = &mut self.readings_buffer;
        let mut readings_buffer = Buffer::new();
        let mut readings_rx = self.readings_rx.take().ok_or("devices: readings_rx already taken")?;
        task::spawn(async move {
            let mut interval = time::interval(Duration::from_secs(1));
            loop {
                tokio::select! {
                    entry = readings_rx.recv() => {
                        if let Some(ReadingsEntry {
                            local_id,
                            device_id,
                            device_label,
                            name,
                            value,
                            nanoseconds,
                        }) = entry {
                            readings_buffer
                                .table("readings").unwrap()
                                .symbol("local_id", &local_id).unwrap()
                                .symbol("device_id", &device_id).unwrap()
                                .symbol("device_label", &device_label).unwrap()
                                .symbol("name", &name).unwrap()
                                .column_f64("value", value).unwrap()
                                .at(nanoseconds).unwrap();
                        } else {
                            break; // Channel closed
                        }
                    },
                    _ = interval.tick() => {
                        if readings_buffer.len() > 0 {
                            debug!("readings_buffer: sending to db");
                            db::send_to_db(&mut readings_buffer);
                        }
                    }
                }
            }
        });

        let _ = task::spawn(async move {
            info!("start_fetching loop");
            let mut last_devices_count = devices_arc.lock().await.len();
            loop {
                let mut devices = devices_arc.lock().await;
                let mut mqtt_manager_guard = mqtt_manager.lock().await;

                mqtt_manager_guard.cleanup_connections().await;

                let device_rows = match client
                    .query("SELECT d.id, d.device_type_id, dt.name AS device_type_name, d.name, d.enabled, d.mqtt_topic, d.label, d.config
                        FROM device d
                        LEFT JOIN device_type dt ON dt.id=d.device_type_id
                    ", &[])
                    .await {
                        Ok(rows) => rows,
                        Err(err) => {
                            if err.is_closed() {
                                debug!("Db connection is closed, trying to acquire a new connection...");
                                match db_pool.get().await {
                                    Ok(new_client) => {
                                        client = new_client;
                                    },
                                    Err(e) => {
                                        error!("Failed to acquire a new connection: {:?}", e);
                                    },
                                };
                            } else {
                                error!("Fetching devices error: {}", err);
                            }
                            time::sleep(Duration::from_secs(5)).await;
                            continue
                        },
                    };

                let mut updated_devices: HashMap<Uuid, Device> = HashMap::new();
                let mut preserve_device_ids: Vec<Uuid> = Vec::new();

                for row in device_rows.iter() {
                    let id: Uuid = row.get("id");
                    let device_type_id: i32 = row.get("device_type_id");
                    let device_type_name: String = row.get("device_type_name");
                    let name: String = row.get("name");
                    let enabled: bool = row.get("enabled");
                    let label: Option<String> = row.get("label");
                    let mqtt_topic: Option<String> = row.get("mqtt_topic");
                    let config_json: Option<String> = row.get("config");

                    let common = DeviceCommon {
                        id,
                        device_type_id,
                        device_type_name,
                        name,
                        enabled,
                        label,
                        mqtt_topic,
                    };

                    let device = Device::from_config(device_type_id, common, config_json);

                    let device = match device {
                        Some(device) => device,
                        None => {
                            error!("Device {} with ID {} was not created properly", Device::get_type_name(device_type_id), id);
                            continue
                        },
                    };

                    // dbg!("Device check {}", &device);

                    if !device.get_common().enabled {
                        continue; // skip disabled devices
                    }

                    // Check if the device already exists and has changed
                    if let Some(existing_device) = devices.get(&id) {
                        if existing_device != &device {
                            info!("Reinitializing {} {}", Device::get_type_name(device_type_id), &id);
                            existing_device.close(&mut mqtt_manager_guard).await; // TODO: refactor reinitialization of a device

                            // initialize existing device when configuration changes or previous initialization failed
                            match device.initialize(&mut mqtt_manager_guard).await {
                                Ok(()) => { updated_devices.insert(id, device); },
                                Err(()) => {
                                    error!("Initialization failed for {} {} ({})", Device::get_type_name(device_type_id), &id, &device.get_common().name);
                                    // preserve old version to repeat initialization in next iteration
                                    preserve_device_ids.push(id.clone());
                                },
                            };
                        } else {
                            preserve_device_ids.push(id.clone()); // preserve unchanged device
                        }
                    } else {
                        info!("New {} device with ID: {}", Device::get_type_name(device_type_id), &device.get_id());
                        match device.initialize(&mut mqtt_manager_guard).await {
                            Ok(()) => { updated_devices.insert(id, device); },
                            Err(()) => {
                                error!("Initialization failed for {} {} ({})", Device::get_type_name(device_type_id), &id, &device.get_common().name);
                                // do not add new device to repeat initialization in next iteration
                            },
                        };
                    }
                }

                // Identify devices to be removed
                let devices_to_remove: Vec<Uuid> = devices.keys().cloned().collect::<Vec<_>>()
                    .iter()
                    .filter(|&key| !updated_devices.contains_key(key) && !preserve_device_ids.contains(key))
                    .cloned()
                    .collect();

                // Remove devices not present in the updated list
                for device_id in devices_to_remove {
                    if let Some(existing_device_to_remove) = devices.get(&device_id) {
                        existing_device_to_remove.close(&mut mqtt_manager_guard).await;
                        devices.remove(&device_id);
                    }
                }

                // Insert or update devices
                devices.extend(updated_devices);

                let new_devices_count = devices.len();
                if !last_devices_count.eq(&new_devices_count) {
                    last_devices_count = new_devices_count;
                    info!("Devices refreshed, current count: {}", &last_devices_count);
                } else {
                    debug!("Devices refreshed, current count: {}", &new_devices_count);
                }

                time::sleep(Duration::from_secs(5)).await;
            }
        }).await;

        // loop { std::thread::yield_now(); }
        loop {
            time::sleep(time::Duration::from_secs(1)).await;
            // check for errors maybe
            // https://users.rust-lang.org/t/what-is-the-idiomatic-way-to-run-a-few-forever-threads-in-parallel/60815/7
        }
    }
}


use std::{collections::BTreeMap, sync::Arc, time::Duration};

use bytes::Bytes;
use questdb::ingress::TimestampNanos;
use rumqttc::QoS;
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};

use crate::db::{self, ReadingsEntry};

use super::{
    mqtt_manager::MqttManager, Device, DeviceBehavior, DeviceCommon,
};

use tokio::{sync::mpsc::Sender, time};

pub const ID: i32 = 1;
pub const NAME: &str = "Victron Cerbo GX";

// Victron device config
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct VictronConfig {
    // specific fields for victron
    mqtt_host: String,
    mqtt_port: u16,
    mqtt_topics_prefix: String,
    mqtt_topics: BTreeMap<String, String>, // using BTreeMap because HashMap doesn't support Hash trait
    mqtt_keepalive_topic: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct Victron {
    pub common: DeviceCommon,
    pub config: VictronConfig,
}

impl DeviceBehavior for Victron {
    fn from_config(
        common: DeviceCommon,
        config_json: Option<&str>,
    ) -> Option<Device> {
        let config = config_json.and_then(|json| serde_json::from_str::<VictronConfig>(json).ok());
        match config {
            Some(config) => Some(Device::Victron(Victron { common, config })),
            None => None,
        }
    }

    async fn initialize(&self, mqtt_manager: &mut MqttManager) -> Result<(), ()> {
        let client = format!(
            "sumon-local-{}",
            std::env::var("LOCAL_ID").expect("Error: LOCAL_ID not found")
        );
        let broker_host = &self.config.mqtt_host;
        let broker_port: u16 = self.config.mqtt_port.clone();
        let broker_id = format!("{}:{}", broker_host, broker_port);
        if self.common.enabled {
            info!("victron: connect to {}:{}", &broker_host, &broker_port);
            // set 0 keep alive because victron broker won't reply
            let (client, shutdown_rx) = mqtt_manager
                .connect(&broker_id, &client, &broker_host, broker_port, Duration::ZERO)
                .await
                .map_err(|_| ())?;
            let client_clone = Arc::clone(&client);

            // subscribe topics
            for topic in self.config.mqtt_topics.keys() {
                let _ = mqtt_manager
                    .subscribe(
                        &broker_id,
                        Device::Victron(self.clone()),
                        &format!("{}{}", &self.config.mqtt_topics_prefix, &topic),
                        QoS::AtLeastOnce, // Reduced Overhead QoS 1
                    )
                    .await;
            }

            // setup keepalive loop so victron publishes required topics
            // https://github.com/victronenergy/dbus-mqtt#detailed-keepalive-behaviour
            // https://github.com/victronenergy/venus/wiki/dbus
            let keepalive_topic = self.config.mqtt_keepalive_topic.clone();
            let topics_json: Value = self
                .config
                .mqtt_topics
                .keys()
                .map(|topic| json!(topic))
                .collect();
            let payload = topics_json.to_string(); // pretty print? serde_json::to_string_pretty(&topics_json).unwrap();
            let _ = tokio::spawn(async move {
                loop {
                    if *shutdown_rx.borrow() {
                        info!("Victron keepalive loop stop");
                        break; // Exit the keep alive publish loop if shutdown signal is received
                    }

                    // use try_publish() for non-blocking I/O, do not retain messages to only pass to current subscribers
                    match client_clone.lock().await.try_publish(
                        &keepalive_topic,
                        QoS::AtMostOnce, // Fire and Forget: With QoS 0
                        false,
                        payload.clone(),
                    ) {
                        Ok(_) => debug!("Victron keepalive OK: {} {}", &keepalive_topic, payload.clone()),
                        Err(err) => error!("victron: Keepalive error: {}", err),
                    };

                    time::sleep(Duration::from_secs(20)).await; // Victron should publish for 60s after keepalive
                }
            });
        }

        Ok(())
    }

    async fn close(&self, mqtt_manager: &mut MqttManager) {
        let broker_host = &self.config.mqtt_host;
        let broker_port: u16 = self.config.mqtt_port.clone();
        let broker_id = format!("{}:{}", broker_host, broker_port);
        let _ = mqtt_manager
            .unsubscribe_device(&broker_id, Device::Victron(self.clone()), false)
            .await;
    }

    async fn incoming(&self, topic: &str, message: &Bytes, readings_tx: &Sender<db::ReadingsEntry>) {
        debug!("Victron: incoming {} '{:?}'", topic, message);
        let topic_without_prefix: &str = topic.strip_prefix(self.config.mqtt_topics_prefix.as_str()).get_or_insert(topic); // remove mqtt_topics_prefix
        if let Some(name) = self.config.mqtt_topics.get(topic_without_prefix) {
            if let Ok(message_str) = std::str::from_utf8(message) {
                match serde_json::from_str::<Message>(message_str) {
                    Ok(parsed_message) => {
                        let local_id = std::env::var("LOCAL_ID").expect("Error: LOCAL_ID not found");
                        let label = match &self.common.label {
                            Some(label) => label,
                            None => "",
                        };
                        match readings_tx.send(ReadingsEntry {
                            local_id: local_id.to_string(),
                            device_id: self.common.id.to_string(),
                            device_label: label.to_string(),
                            name: name.to_string(),
                            value: parsed_message.value,
                            nanoseconds: TimestampNanos::now(),
                        }).await {
                            Ok(_) => {},
                            Err(e) => {error!("victron: send reading error: {}", e)},
                        };
                    },
                    Err(e) => {
                        error!("victron: Failed to parse JSON: {}", e);
                    },
                }
            } else {
                error!("victron: Failed to convert Bytes to &str");
            }
        } else {
            warn!("victron: Topic '{}' not found in mqtt_topics. Ignoring message.", topic);
        }
    }
}

#[derive(Deserialize)]
struct Message {
    value: f64,
}
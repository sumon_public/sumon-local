use dashmap::DashMap;
use rumqttc::{
    AsyncClient, ConnectionError, Event, MqttOptions, Packet, QoS, StateError
};
use std::{
    collections::{HashMap, HashSet}, io::ErrorKind, sync::Arc, time::Duration
};
use tokio::{sync::{mpsc, watch::{self, Receiver, Sender}, Mutex}, time};

use crate::db;

use super::Device;

type ClientValue = ((Arc<Mutex<AsyncClient>>, Sender<bool>, Receiver<bool>), HashMap<Device, HashSet<String>>);
pub struct MqttManager {
    clients: Arc<DashMap<String, ClientValue>>,
    readings_tx: mpsc::Sender<db::ReadingsEntry>,
}

impl MqttManager {
    pub fn new(readings_tx: mpsc::Sender<db::ReadingsEntry>) -> Self {
        MqttManager {
            clients: Arc::new(DashMap::new()),
            readings_tx,
        }
    }

    pub async fn connect(
        &mut self,
        broker_id: &str,
        client_id: &str,
        broker_address: &str,
        port: u16,
        keep_alive: Duration,
    ) -> Result<(Arc<Mutex<AsyncClient>>, Receiver<bool>), rumqttc::StateError> {
        let clients = self.clients.clone();
        if !clients.contains_key(&broker_id.to_string()) {
            let mut mqtt_options = MqttOptions::new(client_id, broker_address, port);
            mqtt_options.set_keep_alive(keep_alive);
            // clean session on connect because we want to reset subscriptions
            // mqtt_options.set_clean_session(true); it is true by default

            let (async_client, mut event_loop) = AsyncClient::new(mqtt_options, 100);
            let async_client = Arc::new(Mutex::new(async_client));

            // create watch channel so device specific tasks can be notified when device is terminated
            // tokio watch channel - multi-producer, multi-consumer. Many values can be sent, but no history is kept
            let (shutdown_tx, shutdown_rx) = watch::channel(false);
            let broker_id_2 = broker_id.to_string();

            clients.insert(broker_id.to_string(), ((async_client, shutdown_tx, shutdown_rx), HashMap::new()));

            let clients_2 = self.clients.clone();
            let mut readings_tx = self.readings_tx.clone();

            tokio::spawn(async move {
                info!("mqtt: starting event_loop.poll() {}", &broker_id_2);
                loop {
                    match event_loop.poll().await {
                        Ok(Event::Incoming(Packet::Publish(p))) => {
                            debug!("mqtt: {} incoming packet publish: {:?}", &broker_id_2, p);

                            // TODO: maybe cache relevant devices to avoid repetitive searches
                            // but since we use DashMap with reduced read locking, it might not be necessary
                            if let Some((_, devices_map)) = clients_2.get(&broker_id_2).as_deref() {
                                for (device, topics) in devices_map.iter() {
                                    if topics.contains(&p.topic) {
                                        device.incoming(&p.topic, &p.payload, &mut readings_tx).await;
                                    }
                                }
                            }
                        }
                        Ok(Event::Incoming(i)) => {
                            // Handle generic incoming messages - only to satisfy compiler
                            debug!("mqtt: {} Loop Incomming: {:?}", &broker_id_2, i);
                        }
                        Ok(Event::Outgoing(o)) => {
                            debug!("mqtt: {} Loop Outgoing: {:?}", &broker_id_2, o);
                        }
                        Err(ConnectionError::MqttState(StateError::Io(io_err))) => {
                            if io_err.kind() == ErrorKind::ConnectionAborted {
                                info!("mqtt: {} connection aborted: MqttState > StateError::Io: {}", &broker_id_2, &io_err);
                                break; // end the loop
                            }
                            error!("mqtt: {} event_loop Error: MqttState > StateError::Io: {}", &broker_id_2, &io_err);
                            error!("mqtt: {} event_loop Error: MqttState > StateError::Io kind: {}", &broker_id_2, &io_err.kind());
                            time::sleep(Duration::from_secs(5)).await;
                        }
                        Err(err) => {
                            error!("mqtt: {} event_loop Error: {:?}", &broker_id_2, err);
                            time::sleep(Duration::from_secs(5)).await;
                        }
                    }
                }
            });
        }

        let client_value = clients.get(broker_id);
        let ((async_client, _shutdown_tx, shutdown_rx), _) = client_value.as_deref()
            .ok_or_else(|| rumqttc::StateError::InvalidState)?;
        Ok((async_client.clone(), shutdown_rx.clone()))
    }

    pub async fn subscribe(
        &mut self,
        broker_id: &str,
        device: Device,
        topic: &str,
        qos: QoS,
    ) -> Result<(), rumqttc::ClientError> {
        let mut client_value = self.clients.get_mut(broker_id);
        let ((client, _, _), subscribed_devices) = client_value.as_deref_mut()
            .ok_or_else(|| {
                rumqttc::ClientError::Request(rumqttc::Request::Subscribe(rumqttc::Subscribe::new(
                    topic, qos,
                )))
            })?;

        if !subscribed_devices.contains_key(&device) {
            subscribed_devices.insert(device.clone(), HashSet::new());
        }

        match subscribed_devices.get_mut(&device) {
            Some(device_topics) => {
                if !device_topics.contains(topic) {
                    client.lock().await.subscribe(topic, qos).await?;
                    device_topics.insert(topic.to_string());
                }
            }
            None => {}
        };

        Ok(())
    }

    pub async fn _unsubscribe(
        &mut self,
        broker_id: &str,
        device: Device,
        topic: &str,
    ) -> Result<(), rumqttc::ClientError> {
        let mut client_value = self.clients.get_mut(broker_id);
        let ((client, _, _), subscribed_devices) = client_value.as_deref_mut().ok_or_else(|| {
            rumqttc::ClientError::Request(rumqttc::Request::Unsubscribe(rumqttc::Unsubscribe::new(
                topic,
            )))
        })?;
        let cloned_subscribed_devices = subscribed_devices.clone();
        match subscribed_devices.get_mut(&device) {
            Some(device_topics) => {
                if device_topics.contains(topic) {
                    device_topics.remove(topic);
                    for (other_device, other_device_topics) in cloned_subscribed_devices.iter() {
                        if !device.eq(other_device) && other_device_topics.contains(topic) {
                            return Ok(()) // do not unsubscribe if the topic is found in another device
                        }
                    }
                    // client.lock().await.unsubscribe(topic).await?; // unsubscribe() hangs with victron, use try_unsubscribe()
                    match client.lock().await.try_unsubscribe(topic) {
                        Ok(_) => debug!("mqtt: unsubscribe() {} successfully unsubscribed {}", broker_id, topic),
                        Err(e) => error!("mqtt: unsubscribe() {} failed to unsubscribe {} error: {}", broker_id, &topic, e),
                    };
                }
            }
            None => {}
        };

        Ok(())
    }

    pub async fn unsubscribe_device(
        &mut self,
        broker_id: &str,
        device: Device,
        unsubscribe_topics: bool,
    ) -> Result<(), rumqttc::ClientError> {
        {
            info!("mqtt: unsubscribe_device() {}", broker_id);
            let mut client_value = self.clients.get_mut(broker_id);
            let ((client, _, _), subscribed_devices) = client_value.as_deref_mut().ok_or_else(|| {
                rumqttc::ClientError::Request(rumqttc::Request::Unsubscribe(rumqttc::Unsubscribe::new(
                    "#",
                )))
            })?;
            let mut topics_for_unsubscribe = vec![];
            let cloned_subscribed_devices = subscribed_devices.clone();
            match subscribed_devices.get_mut(&device) {
                Some(device_topics) => {
                    if unsubscribe_topics {
                        'outer: for topic in device_topics.iter() {
                            // not necessary to remove each topic because we remove the whole device
                            //device_topics.remove(topic);
                            // mqtt unsubscribe topic only if it is not used by other devices
                            for (other_device, other_device_topics) in cloned_subscribed_devices.iter() {
                                if !device.eq(other_device) && other_device_topics.contains(topic) {
                                    continue 'outer; // do not unsubscribe if the topic is found in another device
                                }
                            }
                            topics_for_unsubscribe.push(topic.clone());
                        }
                    }
                    subscribed_devices.remove(&device); // remove device from subscribed devices
                }
                None => {}
            };
            for topic in topics_for_unsubscribe {
                // let _ = client.lock().await.unsubscribe(topic).await; // unsubscribe() hangs with victron, use try_unsubscribe()
                match client.lock().await.try_unsubscribe(&topic) {
                    Ok(_) => debug!("mqtt: unsubscribe_device() {} successfully unsubscribed {}", broker_id, topic),
                    Err(e) => error!("mqtt: unsubscribe_device() {} failed to unsubscribe {} error: {}", broker_id, &topic, e),
                };
            }
        }
        debug!("mqtt: unsubscribe_device() {} cleanup_connections()", broker_id);
        self.cleanup_connections().await;

        Ok(())
    }

    pub async fn cleanup_connections(&mut self) {
        let mut clients_to_remove = Vec::new();
        for mut ref_mut_multi_item in self.clients.iter_mut() {
            let client_id = ref_mut_multi_item.key().clone();
            let (_client, ref mut device_list) = ref_mut_multi_item.value_mut();

            let mut devices_with_empty_topics = Vec::new();
            for (device_id, topics) in device_list.iter() {
                if topics.is_empty() {
                    devices_with_empty_topics.push(device_id.clone());
                }
            }
            // Remove devices with empty topic lists
            for device_id in devices_with_empty_topics {
                device_list.remove(&device_id);
            }
            if device_list.is_empty() {
                clients_to_remove.push(client_id.clone());
            }
        }

        // Remove clients with empty device lists
        for client_id in clients_to_remove {
            let client_value = match self.clients.remove(&client_id) {
                Some((_, client_value)) => client_value,
                None => continue,
            };
            let ((client, shutdown_tx, _), _) = client_value;

            // notify device that is being terminated so it can gracefully end some loop tasks it might have (eg. victron keepalive)
            let _ = shutdown_tx.send(true); // only using (and cloning) original Receiver, therefore send() should be enough
            // client.disconnect();
            debug!("mqtt: Try disconnect {}", &client_id);
            match client.lock().await.try_disconnect() {
                Ok(_) => info!("mqtt: Successfully disconnected {}", &client_id),
                Err(e) => error!("mqtt: Failed to disconnect {}: {}", &client_id, e),
            };
        }
    }
}

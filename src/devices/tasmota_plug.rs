use bytes::Bytes;
use chrono::{DateTime, NaiveDateTime};
use questdb::ingress::TimestampNanos;
use rumqttc::QoS;
use serde::{Deserialize, Serialize};
use serde_with::serde_as;
use tokio::sync::mpsc::Sender;
use std::{str, time::Duration};

use crate::db::{self, ReadingsEntry};

use super::{
    mqtt_manager::MqttManager, Device, DeviceBehavior, DeviceCommon,
};

pub const ID: i32 = 2;
pub const NAME: &str = "Tasmota plug";

// Tasmota plug device config
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct TasmotaPlugConfig {
    // specific fields for tasmota_plug
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct TasmotaPlug {
    pub common: DeviceCommon,
    pub config: TasmotaPlugConfig,
}

impl DeviceBehavior for TasmotaPlug {
    fn from_config(
        common: DeviceCommon,
        config_json: Option<&str>,
    ) -> Option<Device> {
        let config =
            config_json.and_then(|json| serde_json::from_str::<TasmotaPlugConfig>(json).ok());
        match config {
            Some(config) => Some(Device::TasmotaPlug(TasmotaPlug { common, config })),
            None => Some(Device::TasmotaPlug(TasmotaPlug {
                common,
                config: TasmotaPlugConfig {},
            })),
        }
    }

    async fn initialize(&self, mqtt_manager: &mut MqttManager) -> Result<(), ()> {
        let client = format!(
            "sumon-local-{}",
            std::env::var("LOCAL_ID").expect("Error: LOCAL_ID not found")
        );
        let broker_host =
            std::env::var("MQTT_BROKER_HOST").expect("Error: MQTT_BROKER_HOST not found");
        let broker_port: u16 = std::env::var("MQTT_BROKER_PORT")
            .expect("Error: MQTT_BROKER_PORT not found")
            .parse()
            .expect("ERROR: MQTT_BROKER_PORT is not a valid u16");
        let broker_id = format!("{}:{}", broker_host, broker_port);
        if self.common.enabled {
            info!("tasmota_plug: connect to {}:{}", &broker_host, &broker_port);
            let _ = mqtt_manager
                .connect(&broker_id, &client, &broker_host, broker_port, Duration::from_secs(60))
                .await
                .map_err(|_| ())?;
            if let Some(mqtt_topic) = self.common.mqtt_topic.clone() {
                let _ = mqtt_manager
                    .subscribe(
                        &broker_id,
                        Device::TasmotaPlug(self.clone()),
                        &mqtt_topic,
                        QoS::AtLeastOnce, // Reduced Overhead QoS 1
                    )
                    .await;
            }
        }
        Ok(())
    }

    async fn close(&self, mqtt_manager: &mut MqttManager) {
        let broker_host =
            std::env::var("MQTT_BROKER_HOST").expect("Error: MQTT_BROKER_HOST not found");
        let broker_port: u16 = std::env::var("MQTT_BROKER_PORT")
            .expect("Error: MQTT_BROKER_PORT not found")
            .parse()
            .expect("ERROR: MQTT_BROKER_PORT is not a valid u16");
        let broker_id = format!("{}:{}", broker_host, broker_port);
        let _ = mqtt_manager
            .unsubscribe_device(&broker_id, Device::TasmotaPlug(self.clone()), true)
            .await;
    }

    async fn incoming(&self, topic: &str, message: &Bytes, readings_tx: &Sender<db::ReadingsEntry>) {
        debug!("TasmotaPlug: incoming {} '{:?}'", topic, message);
        let local_id = std::env::var("LOCAL_ID").expect("Error: LOCAL_ID not found");
        let label = match &self.common.label {
            Some(label) => label,
            None => "",
        };

        let raw_data = str::from_utf8(&message);
        match raw_data {
            Ok(raw_json) => {
                let plug_sensor_result: Result<PlugSensorData, _> = serde_json::from_str(raw_json);
                match plug_sensor_result {
                    Ok(plug_sensor) => {
                        let nanoseconds = match plug_sensor.time.naive_utc().and_utc().timestamp_nanos_opt() {
                            Some(nanoseconds) => TimestampNanos::new(nanoseconds),
                            None => TimestampNanos::now(),
                        };
                        match readings_tx.send(ReadingsEntry {
                            local_id: local_id.to_string(),
                            device_id: self.common.id.to_string(),
                            device_label: label.to_string(),
                            name: "power".to_string(),
                            value: plug_sensor.energy.power,
                            nanoseconds: nanoseconds,
                        }).await {
                            Ok(_) => {},
                            Err(e) => {error!("tasmota_plug: reading send error: {}", e)},
                        };
                        match readings_tx.send(ReadingsEntry {
                            local_id: local_id.to_string(),
                            device_id: self.common.id.to_string(),
                            device_label: label.to_string(),
                            name: "voltage".to_string(),
                            value: plug_sensor.energy.voltage,
                            nanoseconds: nanoseconds,
                        }).await {
                            Ok(_) => {},
                            Err(e) => {error!("tasmota_plug: reading send error: {}", e)},
                        };

                        // dbg!(&plug_sensor);
                        // dbg!(plug_sensor.time);
                        // dbg!(plug_sensor.energy.power);
                    },
                    Err(e) => {error!("tasmota_plug: Failed to parse JSON: {}", e);}
                }
            },
            Err(e) => {error!("tasmota_plug: Failed to convert Bytes to &str: {}", e);}
        }

    }
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
#[serde_as]
pub struct PlugSensorData {
    #[serde(rename = "Time")]
    #[serde_as(as = "chrono::DateTime<chrono::FixedOffset>")]
    pub time: DateTime<chrono::FixedOffset>,
    #[serde(rename = "ENERGY")]
    pub energy: PlugSensorEnergy,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
#[serde_as]
pub struct PlugSensorEnergy {
    #[serde(rename = "TotalStartTime")]
    #[serde_as(as = "chrono::DateTime<chrono::Utc>")]
    pub total_start_time: NaiveDateTime,
    #[serde(rename = "Total")]
    pub total: f64,
    #[serde(rename = "Yesterday")]
    pub yesterday: f64,
    #[serde(rename = "Today")]
    pub today: f64,
    #[serde(rename = "Period")]
    pub period: Option<f64>,
    #[serde(rename = "Power")]
    pub power: f64,
    #[serde(rename = "ApparentPower")]
    pub apparent_power: f64,
    #[serde(rename = "ReactivePower")]
    pub reactive_power: f64,
    #[serde(rename = "Factor")]
    pub factor: f64,
    #[serde(rename = "Voltage")]
    pub voltage: f64,
    #[serde(rename = "Current")]
    pub current: f64,
}
use futures_util::stream::{SplitSink, SplitStream};
use futures_util::TryStreamExt;
use futures_util::{SinkExt, StreamExt};
use reqwest::{Client, Url};
use serde::{Deserialize, Serialize};
use std::str::FromStr;
use tokio::net::TcpStream;
use tokio::task::spawn;
use tokio::time::{sleep, Duration};
use tokio_tungstenite::{connect_async, tungstenite::Message};
use tokio_tungstenite::{MaybeTlsStream, WebSocketStream};

#[derive(Debug, Serialize, Deserialize)]
struct RequestData {
    method: String,
    resource: String,
    body: String,
    hash: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct ResponseData {
    hash: String,
    status: u16,
    body: String,
}

async fn handle_message(
    ws_split_sink: &mut SplitSink<WebSocketStream<MaybeTlsStream<TcpStream>>, Message>,
    ws_split_stream: &mut SplitStream<
        WebSocketStream<tokio_tungstenite::MaybeTlsStream<tokio::net::TcpStream>>,
    >,
) -> Result<(), Box<dyn std::error::Error>> {
    while let Some(msg) = ws_split_stream.try_next().await? {
        match msg {
            Message::Text(text) => {
                if let Ok(req_data) = serde_json::from_str::<RequestData>(&text) {
                    debug!("ws_client: Received RequestData: {}", req_data.resource);
                    let url =
                        Url::from_str(&format!("http://localhost:8888/{}", req_data.resource))?;
                    let client = Client::new();

                    let req = client
                        .post(url)
                        .header("content-type", "application/json")
                        .body(req_data.body.clone())
                        .build()
                        .unwrap();

                    let task = spawn(async move {
                        let res = client.execute(req).await.unwrap();
                        let response_status = res.status();
                        let body = res.bytes().await.unwrap();
                        ResponseData {
                            hash: req_data.hash.to_string(),
                            status: response_status.as_u16(),
                            body: String::from_utf8(body.to_vec()).unwrap(),
                        }
                    });

                    let res_data = task.await.unwrap();
                    let res_text = serde_json::to_string(&res_data).unwrap();
                    ws_split_sink.send(Message::Text(res_text)).await.unwrap();
                } else if text == "ping" {
                    trace!("ws_client: Received ping, replying with pong");
                    if let Err(e) = ws_split_sink.send(Message::Text("pong".into())).await {
                        error!("Error sending pong message: {}", e);
                    }
                } else {
                    info!("ws_client: Received non-JSON message: {}", text);
                }
            }
            _ => (),
        }
    }
    Ok(())
}

pub async fn connect_websocket(
    proxy_base_url: &str,
    proxy_ws_url: &str,
    concentrator_id: &str,
    private_id: &str,
    is_dev: bool,
) -> Result<(), Box<dyn std::error::Error>> {
    let token = request_token(proxy_base_url, concentrator_id, private_id)
        .await?
        .token;

    let url = format!("{}?token={}", proxy_ws_url, token);

    let (ws_stream, _) = connect_async(url).await?;

    let (mut write, mut read) = ws_stream.split();
    let handle = tokio::spawn(async move {
        let _ = handle_message(&mut write, &mut read).await;

        let mut error_counter = 0;
        loop {
            sleep(Duration::from_secs(if is_dev { 1 } else { 15 })).await;
            let ping_result = write.send(Message::Text("ping".into())).await;
            if let Err(ping_err) = ping_result {
                error_counter = error_counter + 1;
                if error_counter >= if is_dev { 1 } else { 1 } {
                    return Box::new(ping_err);
                }
                continue;
            }
            error_counter = 0;
        }
    });

    return Err(handle.await?);
}

#[derive(Debug, Serialize)]
struct TokenRequestData {
    concentrator_id: String,
    private_id: String,
}

#[derive(Debug, Deserialize)]
struct TokenResponseData {
    token: String,
}

async fn request_token(
    proxy_base_url: &str,
    concentrator_id: &str,
    private_id: &str,
) -> Result<TokenResponseData, reqwest::Error> {
    let url = format!("{}ws-token/", proxy_base_url);
    let req_data = TokenRequestData {
        concentrator_id: concentrator_id.to_string(),
        private_id: private_id.to_string(),
    };
    debug!("ws_client: request url: {}", url);
    let client = reqwest::Client::new();
    let resp = client
        .post(url)
        .json(&req_data)
        .send()
        .await?
        .json::<TokenResponseData>()
        .await?;

    Ok(resp)
}
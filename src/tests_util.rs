use futures_util::future::BoxFuture;
use postgres_types::ToSql;
use tokio_postgres::Row;
use std::ops::Deref;

pub trait DbClient {
    fn query_one<'a>(
        &'a self,
        query: &'a str,
        params: &'a [&'a (dyn ToSql + Sync)]
    ) -> BoxFuture<'a, Result<Row, tokio_postgres::Error>>;
}

impl DbClient for deadpool_postgres::Object {
    fn query_one<'a>(
        &'a self,
        query: &'a str,
        params: &'a [&'a (dyn ToSql + Sync)]
    ) -> BoxFuture<'a, Result<Row, tokio_postgres::Error>> {
        Box::pin(async move {
            let client = self.deref();
            client.query_one(query, params).await
        })
    }
}

#[cfg(test)]
#[allow(dead_code)]
pub struct MockDbClient;

#[cfg(test)]
#[allow(unused_variables)]
impl DbClient for MockDbClient {
    fn query_one<'a>(
        &'a self,
        query: &'a str,
        params: &'a [&'a (dyn ToSql + Sync)]
    ) -> BoxFuture<'a, Result<Row, tokio_postgres::Error>> {
        unimplemented!("Implement mocked behavior for tests")
    }
}
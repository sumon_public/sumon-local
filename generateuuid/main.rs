use uuid::Uuid;

fn main() {
    println!("Generating ten uuids:");
    for _ in 0..10 {
        println!("{:?}", Uuid::new_v4());
    }
}

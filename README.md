# Sumon-local

## Run dev local concentrator
```shell
cargo run
```
Concentrator API runs on (http://localhost:8888)

## Generate own LOCAL_ID, PRIVATE_ID
Following command generates 10 UUIDs v4. Choose some for your LOCAL_ID and PRIVATE_ID
```shell
cargo run --bin generateuuid
```

## Update production image and display logs
```shell
docker compose pull sumon-local
docker compose up -d
docker compose logs -f sumon-local
```

## Example `docker-compose.yml` for Arm64 Odroid device

Create `data/mosquitto/config/mosquitto.conf` file with this content:
```conf
persistence true
persistence_location /mosquitto/data/
log_dest file /mosquitto/log/mosquitto.log
allow_anonymous true
listener 1883 0.0.0.0
socket_domain ipv4

# log_type all
# connection_messages true
# log_timestamp true
```

`docker-compose.yml` file:
```yaml
networks:
  default:
    ipam:
      config:
        - subnet: 192.78.0.0/16
 
services:
  sumon-local:
    container_name: sumon-local
    image: some-private-registry/sumon/sumon-local:arm64
    restart: always
    ports:
      - 80:8888
    environment:
      DATABASE_URL: "postgresql://admin:quest@sumon-concentrator-db:8812/questdb"
      QUESTDB_INGRESS_HOST: "sumon-concentrator-db"
      QUESTDB_INGRESS_PORT: "9009"
      PROXY_BASE_URL: "https://some-proxy-url/_/"
      PROXY_WS_URL: "wss://some-proxy-url/_/ws/"
      LOCAL_ID: "local-uuid-of-this-installation"
      PRIVATE_ID: "private-uuid-of-this-installation"
      MQTT_BROKER_HOST: "sumon-concentrator-mosquitto"
      MQTT_BROKER_PORT: "1883"
      RUST_LOG: "info,actix_web=info,actix_server=trace,deadpool.postgres=error"
      LISTEN_IP: "127.0.0.1"
      LISTEN_PORT: "8888"
 
  sumon-concentrator-db:
    container_name: sumon-concentrator-db
    image: questdb/questdb:8.0.1
    restart: always
    volumes:
      - ./data/concentrator-db:/var/lib/questdb:z
    ports:
      - 9000:9000 # for the REST API and the Web Console (accessible at localhost:9000)
      - 8812:8812 # Postgres wire protocol
      - 9009:9009 # InfluxDB line protocol
 
  sumon-concentrator-mosquitto:
    container_name: sumon-concentrator-mosquitto
    image: eclipse-mosquitto:2.0.18
    restart: always
    volumes:
      - ./data/mosquitto/config/:/mosquitto/config/:z
      - ./data/mosquitto/log/:/mosquitto/log/:z
      - ./data/mosquitto/data/:/mosquitto/data/:z
    ports:
      - 1883:1883
      - 9001:9001
```

## Run docker image locally
```shell
docker run -p127.0.0.1:8888:8888 --network sumon_default -eDATABASE_URL="postgresql://admin:quest@sumon-concentrator-db:8812/questdb" \
-eQUESTDB_INGRESS_HOST="sumon-concentrator-db" -eQUESTDB_INGRESS_PORT="9009" \
-ePROXY_BASE_URL="https://some-proxy-url/_/" -ePROXY_WS_URL="wss://some-proxy-url/_/ws/" \
-eLOCAL_ID="0e6eea14-1b67-437c-a337-434fb18d6dc1" -ePRIVATE_ID="ce42219c-8227-480f-96f1-8fd4bf77a7ad" \
-eMQTT_BROKER_HOST="sumon-concentrator-mosquitto" -eMQTT_BROKER_PORT="1883" \
-eRUST_LOG="info,actix_web=info,actix_server=trace,deadpool.postgres=error" \
-eLISTEN_IP="127.0.0.1" -eLISTEN_PORT="8888" -eRUST_BACKTRACE="full" some-private-registry/sumon/sumon-local:latest
```

Replace CMD with `/bin/bash` to be able to run app manualy from within the container
```shell
docker run -it -p127.0.0.1:8888:8888 --network sumon_default -eDATABASE_URL="postgresql://admin:quest@sumon-concentrator-db:8812/questdb" \
-eQUESTDB_INGRESS_HOST="sumon-concentrator-db" -eQUESTDB_INGRESS_PORT="9009" \
-ePROXY_BASE_URL="https://some-proxy-url/_/" -ePROXY_WS_URL="wss://some-proxy-url/_/ws/" \
-eLOCAL_ID="0e6eea14-1b67-437c-a337-434fb18d6dc1" -ePRIVATE_ID="ce42219c-8227-480f-96f1-8fd4bf77a7ad" \
-eMQTT_BROKER_HOST="sumon-concentrator-mosquitto" -eMQTT_BROKER_PORT="1883" \
-eRUST_LOG="info,actix_web=info,actix_server=trace,deadpool.postgres=error" \
-eLISTEN_IP="127.0.0.1" -eLISTEN_PORT="8888" -eRUST_BACKTRACE="full" some-private-registry/sumon/sumon-local:latest /bin/bash
```

## Test arm64 docker image
Prepare qemu and docker (qemu must be installed on the system `pacman -S qemu-full qemu-system-aarch64`)
```shell
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
```
Run docker image with `--platform arm64`. Then inside the container `/usr/local/bin/sumon-local`
```shell
docker run -it -p127.0.0.1:8888:8888 --network sumon_default \
-eDATABASE_URL="postgresql://admin:quest@sumon-concentrator-db:8812/questdb" -eQUESTDB_INGRESS_HOST="sumon-concentrator-db" \
-eQUESTDB_INGRESS_PORT="9009" -ePROXY_BASE_URL="https://some-proxy-url/_/" \
-ePROXY_WS_URL="wss://some-proxy-url/_/ws/" -eLOCAL_ID="0e6eea14-1b67-437c-a337-434fb18d6dc1" \
-ePRIVATE_ID="ce42219c-8227-480f-96f1-8fd4bf77a7ad" -eMQTT_BROKER_HOST="sumon-concentrator-mosquitto" \
-eMQTT_BROKER_PORT="1883" -eRUST_LOG="info,actix_web=info,actix_server=trace,deadpool.postgres=error" \
-eLISTEN_IP="127.0.0.1" -eLISTEN_PORT="8888" -eRUST_BACKTRACE="full" \
--platform arm64 some-private-registry/sumon/sumon-local:arm64 /bin/bash
```